
import time

import pytest
from locators import accordian_service_loc, mainpage_loc
from locators.elements_locators import (broken_links_loc, buttons_loc,
                                        checkbox_loc, dynamic_properties_loc,
                                        links_loc, radiobutton_loc,
                                        textbox_loc, webtables_loc)
from pages.elements_page import ElementsPage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select


class TestElementsPage:
    
    '''
Кейс №4: Валидные данные Text Box
    1. Открыть категорию Elements, перейти в Text Box
    2. Заполнить поле(-я) валидными данными
    3. Проверить отправку данных в появившемся блоке под формой
    '''
    def test_04_textbox_correct_input(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.TEXTBOX_ITEM).click()
        
        elements_page.fill_input_located(*textbox_loc.FULL_NAME_INPUT, textbox_loc.full_name)
        elements_page.fill_input_located(*textbox_loc.EMAIL_INPUT, textbox_loc.email)
        elements_page.fill_input_located(*textbox_loc.CURRENT_ADDRESS_INPUT, textbox_loc.current_address)
        elements_page.fill_input_located(*textbox_loc.PERMANENT_ADDRESS_INPUT, textbox_loc.permanent_address)
        elements_page.browser.find_element(*textbox_loc.SUBMIT_BUTTON).click()
        
        actual_name = elements_page.browser.find_element(*textbox_loc.NAME_LOCATOR).text
        expected_name = textbox_loc.full_name
        assert expected_name in actual_name, f'There is no "{expected_name}" in text box output, '\
            f'actual output: "{actual_name}"'
        
        actual_email = elements_page.browser.find_element(*textbox_loc.EMAIL_LOCATOR).text
        expected_email = textbox_loc.email
        assert expected_email in actual_email, f'There is no "{expected_email}" in text box output, '\
            f'actual output: "{actual_email}"'

        actual_cur_address = elements_page.browser.find_element(*textbox_loc.CURRENT_ADDRESS_LOCATOR).text
        expected_cur_address = textbox_loc.current_address
        assert expected_cur_address in actual_cur_address, f'There is no "{expected_cur_address}" '\
            f'in textbox output, actual output: "{actual_cur_address}"'

        actual_perm_address = elements_page.browser.find_element(*textbox_loc.PERMANENT_ADDRESS_LOCATOR).text
        expected_perm_address = textbox_loc.permanent_address
        assert expected_perm_address in actual_perm_address, f'There is no "{expected_perm_address}" '\
            f'in text box output, actual output: "{actual_perm_address}"'


    '''
Кейс №5: Невалидные данные Text Box
    1. Открыть категорию Elements, перейти в Text Box
    2. Заполнить поле(-я) невалидными данными
    3. Проверить отсутствие данных под формой и выделение поля(-ей) с ошибками
    '''
    def test_05_textbox_error(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.TEXTBOX_ITEM).click()

        elements_page.fill_input_located(*textbox_loc.EMAIL_INPUT, textbox_loc.invalid_email)
        elements_page.browser.find_element(*textbox_loc.SUBMIT_BUTTON).click()

        email_input = elements_page.browser.find_element(*textbox_loc.EMAIL_INPUT)
        attribute = textbox_loc.error_attribute
        value = textbox_loc.error_attribute_value
        email_input_classes = email_input.get_attribute(attribute).split()
        
        assert elements_page.element_has_attribute_value(email_input, attribute, value), 'Email '\
            f'input has no {attribute} {value}, classes of it: {email_input_classes}'


    '''
Кейс №6: Выделение нужных чекбоксов
    1. Открыть категорию Elements, перейти в Check Box
    2. Выделить следующие чекбоксы: Notes, Angular, General, Word File.doc
    3. Проверить выделенные чекбоксы в блоке «You have selected»
    '''
    def test_06_checkboxes(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.CHECKBOX_ITEM).click()

        elements_page.select_checkboxes(checkbox_loc.checkbox_titles_to_select)
        
        expected_titles = checkbox_loc.expected_titles
        selected_titles = elements_page.get_titles_list_located(*checkbox_loc.SELECTED_TITLES)
        assert selected_titles == expected_titles, elements_page.error_message('selected checkboxes list', selected_titles, expected_titles)

    
    '''
Кейс №7: Выделение нужных радио-кнопок
    1. Открыть категорию Elements, перейти в Radio Button
    2. Выделить Yes
    3. Проверить выделенную кнопку в блоке «You have selected»
    4. Проверить что кнопку No невозможно выделить
    5. Выделить Impressive
    6. Проверить выделенную кнопку в блоке «You have selected»
    '''
    def test_07_radiobuttons(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.RADIOBUTTON_ITEM).click()
        
        yes_radio = elements_page.browser.find_element(*radiobutton_loc.YES_RADIOBUTTON)
        impressive_radio = elements_page.browser.find_element(*radiobutton_loc.IMPRESSIVE_RADIOBUTTON)
        no_radio = elements_page.browser.find_element(*radiobutton_loc.NO_RADIOBUTTON)

        yes_radio.click()
        selected_radiobutton_title = elements_page.browser.find_element(*radiobutton_loc.SELECTED_RADIOBUTTON_TITLE).text
        assert selected_radiobutton_title == 'Yes', elements_page.error_message('radiobutton is selected', selected_radiobutton_title, 'Yes')

        impressive_radio.click()
        selected_radiobutton_title = elements_page.browser.find_element(*radiobutton_loc.SELECTED_RADIOBUTTON_TITLE).text
        assert selected_radiobutton_title == 'Impressive', elements_page.error_message('radiobutton is selected', selected_radiobutton_title, 'Impressive')

        attribute_to_check = radiobutton_loc.disabled_attribute_to_check
        attribute_value = radiobutton_loc.disabled_value
        assert elements_page.element_has_attribute_value(no_radio, attribute_to_check, attribute_value), f'"No" radiobutton has no {attribute_to_check} {attribute_value}, it\'s {attribute_to_check}\': '\
            f'{impressive_radio.get_attribute(attribute_to_check).split()}'


    '''
Кейс №8: Добавление записи в таблицу
    1. Открыть категорию Elements, перейти в Web Tables
    2. Добавить новую запись в таблицу с валидными данными
    3. Проверить добавление записи согласно введенным данным
    '''    
    def test_08_adding_row(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.WEBTABLE_ITEM).click()

        elements_page.add_rows(webtables_loc.row_to_add)
        actual_row = elements_page.get_row(*webtables_loc.FORTH_ROW)

        assert actual_row == webtables_loc.row_to_add, elements_page.error_message('data in row', actual_row, webtables_loc.row_to_add)
        
        
    '''
Кейс №9: Редактирование записей в таблице
    1. Открыть категорию Elements, перейти в Web Tables
    2. Заменить данные первой записи в таблице на данные из третьей записи
    3. Заменить данные второй записи в таблице на старые данные из первой записи
    4. Заменить данные третьей записи в таблице на старые данные из второй записи
    5. Проверить содержание записей согласно их новому расположению

    > p.s. если коротко, то записи меняются так 3=>1=>2=>3
    '''
    def test_09_mixing_rows(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.WEBTABLE_ITEM).click()

        first_row = expected_second_row = elements_page.get_row(*webtables_loc.FIRST_ROW)
        second_row = expected_third_row = elements_page.get_row(*webtables_loc.SECOND_ROW)
        third_row = expected_first_row = elements_page.get_row(*webtables_loc.THIRD_ROW)

        elements_page.edit_row(1, third_row)
        elements_page.edit_row(2, first_row)
        elements_page.edit_row(3, second_row)

        actual_first_row = elements_page.get_row(*webtables_loc.FIRST_ROW)
        actual_second_row = elements_page.get_row(*webtables_loc.SECOND_ROW)
        actual_third_row =  elements_page.get_row(*webtables_loc.THIRD_ROW)

        assert actual_first_row == expected_first_row, elements_page.error_message('data in row', actual_first_row, expected_first_row)

        assert actual_second_row == expected_second_row, elements_page.error_message('data in row', actual_second_row, expected_second_row)

        assert actual_third_row == expected_third_row, elements_page.error_message('data in row', actual_third_row, expected_third_row)

    
    '''
Кейс №10: Удаление записи в таблице
    1. Открыть категорию Elements, перейти в Web Tables
    2. Удалить одну из записей в таблице
    3. Проверить изменения таблицы
    '''
    def test_10_deleting_row(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.WEBTABLE_ITEM).click()

        filled_rows_list = elements_page.browser.find_elements(*webtables_loc.NOT_EMPTY_ROW)
        number_of_rows_before_deletion = len(filled_rows_list) - 1
        elements_page.browser.find_element(*webtables_loc.DELETE_SECOND_ROW_BUTTON).click()
        actual_number_of_rows = len(elements_page.browser.find_elements(*webtables_loc.NOT_EMPTY_ROW)) - 1

        assert actual_number_of_rows == (number_of_rows_before_deletion - 1), elements_page.error_message('number of rows afer deletion', number_of_rows_before_deletion - 1, actual_number_of_rows)

    
    '''
Кейс №11: Пагинация таблицы через кнопки
    1. Открыть категорию Elements, перейти в Web Tables
    2. Добавить такое количество записей, чтобы их было >5
    3. Переключить отображение только для 5 записей
    4. Проверить наличие записей согласно введенным данным, переключив страницу на следующую
    '''
    def test_11_pagination_from_buttons(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.WEBTABLE_ITEM).click()
        
        elements_page.add_rows(webtables_loc.rows_to_add)
        select = Select(elements_page.browser.find_element(*webtables_loc.ROWS_PER_PAGE_SELECT))
        select.select_by_value('5')
        elements_page.browser.find_element(*webtables_loc.NEXT_BUTTON).click()

        actual_last_row = elements_page.get_row(*webtables_loc.FIRST_ROW)

        assert actual_last_row == webtables_loc.rows_to_add[-1], elements_page.error_message('data in row', actual_last_row, webtables_loc.rows_to_add[-1])


    '''
Кейс №12: Пагинация таблицы через значения
    1. Открыть категорию Elements, перейти в Web Tables
    2. Добавить такое количество записей, чтобы их было >5
    3. Переключить отображение только для 5 записей
    4. Переключить страницу отображения записей на следующую
    5. Указать номер страницы «1» и перейти
    6.  Проверить наличие записей согласно введенным данным
    '''
    def test_12_pagination_from_values(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.WEBTABLE_ITEM).click()

        elements_page.add_rows(webtables_loc.rows_to_add)
        select = Select(elements_page.browser.find_element(*webtables_loc.ROWS_PER_PAGE_SELECT))
        select.select_by_value('5')
        elements_page.browser.find_element(*webtables_loc.NEXT_BUTTON).click()
        
        page_number_input = elements_page.browser.find_element(*webtables_loc.PAGE_NUMBER_INPUT)
        elements_page.clear_input(page_number_input)
        elements_page.fill_input(page_number_input, '1')
        page_number_input.send_keys(Keys.ENTER)

        actual_forth_row = elements_page.get_row(*webtables_loc.FORTH_ROW)
        actual_last_row = elements_page.get_row(*webtables_loc.LAST_ROW)

        assert actual_forth_row == webtables_loc.rows_to_add[0], elements_page.error_message('data in row', actual_forth_row, webtables_loc.rows_to_add[0])
        
        assert actual_last_row == webtables_loc.rows_to_add[1], elements_page.error_message('data in row', actual_last_row, webtables_loc.rows_to_add[1])


    '''
Кейс №13: Клики по кнопкам
    1. Открыть категорию Elements, перейти в Buttons
    2. Поочередно нажать кнопки: Right Click, Click, Double Click
    3. Проверить нажатие кнопок в блоке под ними «You have done»
    '''
    def test_13_clicks(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.BUTTONS_ITEM).click()

        elements_page.browser.find_element(*buttons_loc.CLICK_ME_BUTTON).click()
        actual_message = elements_page.browser.find_element(*buttons_loc.CLICK_MESSAGE).text
        
        assert actual_message == buttons_loc.click_message, elements_page.error_message('message', actual_message, buttons_loc.click_message)

        right_click_button = elements_page.browser.find_element(*buttons_loc.RIGHT_CLICK_BUTTON)
        elements_page.actions.context_click(right_click_button).perform()
        actual_message = elements_page.browser.find_element(*buttons_loc.RIGHT_CLICK_MESSAGE).text
        
        assert actual_message == buttons_loc.right_click_message, elements_page.error_message('message', actual_message, buttons_loc.right_click_message)
        
        double_click_button = elements_page.browser.find_element(*buttons_loc.DOUBLE_CLICK_BUTTON)
        elements_page.actions.double_click(double_click_button).perform()
        actual_message = elements_page.browser.find_element(*buttons_loc.DOUBLE_CLICK_MESSAGE).text
        
        assert actual_message == buttons_loc.double_click_message, elements_page.error_message('message', actual_message, buttons_loc.double_click_message)


    '''
Кейс №14: Новая вкладка
    1. Открыть категорию Elements, перейти в Links
    2. Перейти по ссылке «Home», откроется новая вкладка
    3. Проверить, что текущее состояние адресной строки соответствует «https://demoqa.com/»
    4. Переключиться на первую вкладку (не закрывая новую!) и проверить, что текущее состояние адресной строки соответствует «https://demoqa.com/links»
    '''
    def test_14_tab_switching(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.LINKS_ITEM).click()

        elements_page.browser.find_element(*links_loc.HOME_LINK).click()
        elements_page.switch_to_new_tab()
        
        elements_page.check_current_url(mainpage_loc.LINK)


    '''
Кейс №15 (API): Проверка статусов запросов
    1. Открыть категорию Elements, перейти в Links
    2. Проверить правильность работы каждого запроса (Created, No Content, Moved, Bad Request, Unauthorized, Forbidden, Not Found):
    3. Кликнуть на линк
    4. Получить запрос и проверить его имя и код статуса
    '''
    @pytest.mark.parametrize('link_locator', links_loc.links_to_click)
    def test_15_request_status(self, browser, link_locator):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.LINKS_ITEM).click()

        elements_page.browser.find_element(*link_locator).click()
        counter = links_loc.links_to_click.index(link_locator)
        expected_status = links_loc.expected_statuses[counter]
        expected_text = links_loc.expected_texts[counter]
        
        status_and_text_elements = elements_page.browser.find_elements(*links_loc.RESPONSE_STATUS_AND_TEXT)
        actual_status = status_and_text_elements[0].text
        actual_text = status_and_text_elements[1].text
        
        assert actual_status == expected_status, elements_page.error_message('status', actual_status, expected_status)
        assert actual_text == expected_text, elements_page.error_message('text', actual_text, expected_text)


    '''
Кейс №16: Картинки
    1. Открыть категорию Elements, перейти в Broken Links — Images
    2. Проверить отображение картинки блока Valid image
    3. Проверить неотображение картинки блока Broken image
    '''      
    def test_16_broken_and_unbroken_images(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.BROKEN_LINKS_ITEM).click()
        
        assert not elements_page.image_is_broken(*broken_links_loc.VALID_IMAGE), 'Image is broken'
        
        assert elements_page.image_is_broken(*broken_links_loc.BROKEN_IMAGE), 'Image must be broken'


    '''
Кейс №17: Ссылка
    1. Открыть категорию Elements, перейти в Broken Links — Images
    2.  Нажать на ссылку «Click Here for Broken Link»
    3. Проверить переход на новую страницу и её статус (500)
    4. Вернуться назад на предыдущую страницу
    5. Нажать на ссылку «Click Here for Valid Link»
    6. Проверить, что текущее состояние адресной строки соответствует «https://demoqa.com/»
    '''
    def test_17_valid_and_broken_links(self, browser):
        elements_page = ElementsPage(browser)
        elements_page.open()
        elements_page.browser.find_element(*accordian_service_loc.BROKEN_LINKS_ITEM).click()

        elements_page.browser.find_element(*broken_links_loc.BROKEN_LINK).click()
        status_message = elements_page.browser.find_element(*broken_links_loc.MESSAGE_WITH_STATUS)
        assert broken_links_loc.status_to_check in status_message.text, elements_page.error_message('status', status_message.text, broken_links_loc.status_to_check)

        elements_page.browser.back()
        elements_page.browser.find_element(*broken_links_loc.VALID_LINK).click()
        elements_page.check_current_url(mainpage_loc.LINK)


    '''
Кейс №18: Динамические свойства объектов
    1. Открыть категорию Elements, перейти в Dynamic Properties
    2.  Проверить состояние кнопок:
	    2.1 Кнопка «Will enable 5 seconds» должна быть неактивной первые пять секунд
	    2.2 В кнопке «Color Change» должен измениться цвет текста через пять секунд
	    2.3 Кнопка «Visible after 5 seconds» должна быть невидимой первые пять секунд
    '''
    def test_18_dynamic_properties(self, browser):
        elements_page = ElementsPage(browser, wait_timeout=6)
        elements_page.open()
        elements_page.scroll_to_avoid_footer(elements_page.browser.find_element(*accordian_service_loc.DYNAMIC_PROPERTIES_ITEM))
        elements_page.browser.find_element(*accordian_service_loc.DYNAMIC_PROPERTIES_ITEM).click()

        assert elements_page.is_clickable(*dynamic_properties_loc.DELAYED_CLICKABILITY_BUTTON), 'Button is not clickable'

        assert elements_page.is_visible(*dynamic_properties_loc.DELAYED_VISIBILITY_BUTTON), 'Button is not visible'
        
        change_color_button = elements_page.browser.find_element(*dynamic_properties_loc.COLOR_CHANGE_BUTTON)
        counter = 0
        result = False
        while counter <= 60:       
            current_color = change_color_button.value_of_css_property('color')
            if current_color == dynamic_properties_loc.expected_color:
                result = True
                break
            else:
                counter += 1
                time.sleep(0.1)

        assert result, 'Button is not red'



        

 

