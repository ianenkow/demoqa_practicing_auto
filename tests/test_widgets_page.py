
import time

import pytest
from locators import accordian_service_loc
from locators.widgets_locators import (accordian_loc, autocomplete_loc,
                                       datepicker_loc, menu_loc,
                                       progressbar_loc, slider_loc, tabs_loc,
                                       tooltips_loc)
from pages.widgets_page import WidgetsPage
from selenium.webdriver.support import expected_conditions as EC


class TestWidgetsPage:
    
    '''
Кейс №28: Гармошка
    1. Открыть категорию Widgets, перейти в Accordian
    2. Поочередно открывать каждый блок в гармошке и проверять наличие нужного текста и отсутствие ненужного

    > p.s. в 1 блоке не должно быть текста 2 и 3, во 2 нет текста 1 и 3, в 3 нет текста 1 и 2
    '''
    def test_28_harmonic(self, browser):
        widgets_page = WidgetsPage(browser)
        widgets_page.open()
        widgets_page.browser.find_element(*accordian_service_loc.ACCORDIAN_ITEM).click()     

        widgets_page.browser.find_element(*accordian_loc.SECOND_CARD_HEADER).click()
        actual_second_card_text = widgets_page.get_expanded_card_text()
        assert actual_second_card_text == accordian_loc.expected_second_card_text, widgets_page.error_message('second card text', actual_second_card_text, accordian_loc.expected_second_card_text)

        widgets_page.browser.find_element(*accordian_loc.THIRD_CARD_HEADER).click()
        actual_third_card_text = widgets_page.get_expanded_card_text()
        assert actual_third_card_text == accordian_loc.expected_third_card_text, widgets_page.error_message('third card text', actual_third_card_text, accordian_loc.expected_third_card_text)

        widgets_page.browser.find_element(*accordian_loc.FIRST_CARD_HEADER).click()
        actual_first_card_text = widgets_page.get_expanded_card_text()
        assert actual_first_card_text == accordian_loc.expected_first_card_text, widgets_page.error_message('first card text', actual_first_card_text, accordian_loc.expected_first_card_text)


    '''
Кейс №29: Автокомплит нескольких слов
    1. Открыть категорию Widgets, перейти в Auto Complite
    2. В поле «Type multiple color names» поочередно ввести и выбрать следующее (не удаляя предыдущие):
	    2.1 «re» и выбрать red
	    2.2 «r» и выбрать purple
	    2.3 «q» и выбрать aqua
    3. Проверить слова и их последовательность в поле
    '''
    def test_29_multiple_autocomplete(self, browser):
        widgets_page = WidgetsPage(browser)
        widgets_page.open()
        widgets_page.browser.find_element(*accordian_service_loc.AUTO_COMPLETE_ITEM).click()

        widgets_page.browser.find_element(*autocomplete_loc.MULTIPLE_COLORS_INPUT).send_keys('re')
        widgets_page.choose_color('Red')
        widgets_page.browser.find_element(*autocomplete_loc.MULTIPLE_COLORS_INPUT).send_keys('r')
        widgets_page.choose_color('Purple')
        widgets_page.browser.find_element(*autocomplete_loc.MULTIPLE_COLORS_INPUT).send_keys('q')
        widgets_page.choose_color('Aqua')

        actual_colors = widgets_page.get_titles_list_located(*autocomplete_loc.MULTIPLE_COLORS_INPUT_CHOSEN_COLORS)
        assert actual_colors == autocomplete_loc.expected_colors, widgets_page.error_message('chosen colors', actual_colors, autocomplete_loc.expected_colors)


    '''
Кейс №30: Автокомплит одного слова
    1. Открыть категорию Widgets, перейти в Auto Complite
    2. В поле «Type single color name» ввести «b» и выбрать blue
    3. В это же поле ввести «d» и выбрать indigo
    4. Проверить поле на наличие последнего слова и отсутствие первого
    '''
    def test_30_single_autocomplete(self, browser):
        widgets_page = WidgetsPage(browser)
        widgets_page.open()
        widgets_page.browser.find_element(*accordian_service_loc.AUTO_COMPLETE_ITEM).click()
        

        widgets_page.browser.find_element(*autocomplete_loc.SINGLE_COLOR_INPUT).send_keys('b')
        widgets_page.choose_color('Blue')
        widgets_page.browser.find_element(*autocomplete_loc.SINGLE_COLOR_INPUT).send_keys('d')
        widgets_page.choose_color('Indigo')

        actual_colors = widgets_page.get_text_from_paragraphs_located(*autocomplete_loc.SINGLE_COLOR_INPUT_CHOSEN_COLOR)
        assert autocomplete_loc.expected_color in actual_colors and autocomplete_loc.not_expected_color not in actual_colors, widgets_page.error_message('color chosen', actual_colors, autocomplete_loc.expected_color)


    '''
Кейс №31: Дата и время
    1. Открыть категорию Widgets, перейти в Date Picker
    2. В поле «Date And Time» выбрать дату: 27 ноября 1940 года 12:00
    3. Закрыть блок выбора даты и времени
    4. Проверить данные в поле на соответствие введенным данным
    '''
    def test_31_date_and_time(self, browser):
        widgets_page = WidgetsPage(browser)
        widgets_page.open()
        widgets_page.browser.find_element(*accordian_service_loc.DATE_PICKER_ITEM).click()

        widgets_page.browser.find_element(*datepicker_loc.DATE_AND_TIME_INPUT).click()
        widgets_page.set_date('27.11.1940')
        widgets_page.set_time('12:00')
        
        date_and_time_element = widgets_page.browser.find_element(*datepicker_loc.DATE_AND_TIME_INPUT_TEXT)
        chosen_date_and_time = date_and_time_element.get_attribute('value')

        assert chosen_date_and_time == datepicker_loc.expected_date_and_time, widgets_page.error_message('date and time chosen', chosen_date_and_time, datepicker_loc.expected_date_and_time)


    '''
Кейс №32: Слайдер
    1. Открыть категорию Widgets, перейти в Slider
    2. Установить значение в слайдере на 72
    3. Проверить установленное значение
    '''
    def test_32_slider(self, browser):
        widgets_page = WidgetsPage(browser)
        widgets_page.open()
        widgets_page.browser.find_element(*accordian_service_loc.SLIDER_ITEM).click()

        slider = widgets_page.browser.find_element(*slider_loc.SLIDER)
        max_value = int(slider.get_attribute('max'))
        
        # max_value/2 is a center of slider, click will be located there
        # raw_value is negative when we need to drag slider left
        raw_value = slider_loc.expected_slider_value - max_value / 2
    
        # we should add shift when moving slider left and add substract it when moving right
        # so we are multiplying it by -1 as it's opposite to raw_value
        # 0.2 is constant that can be counted in element's attributes
        shift = 0.2 * raw_value * (-1)

        #as click will be made on the center of the slider, the final formula is
        offset = slider.size['width'] * raw_value/max_value + shift
        widgets_page.actions.move_to_element(slider).click_and_hold(slider).move_by_offset(offset, 0).release().perform()
        
        actual_slider_value = widgets_page.browser.find_element(*slider_loc.SLIDER_VALUE_BOX).get_attribute('value')
        assert actual_slider_value == str(slider_loc.expected_slider_value), widgets_page.error_message('slider value', actual_slider_value, slider_loc.expected_slider_value)


    '''
Кейс №33: Индикатор - старт
    1. Открыть категорию Widgets, перейти в Progress Bar
    2. Нажать кнопку Start
    3. Проверить изменение индикатора

Кейс №34: Индикатор - стоп
    1. Открыть категорию Widgets, перейти в Progress Bar
    2. Нажать кнопку Start
    3. Дождаться индикатор до состояния <100%
    4. Нажать кнопку Stop
    5. Проверить изменение индикатора
    '''
    def test_33_34_progress_bar(self, browser):
        widgets_page = WidgetsPage(browser)
        widgets_page.open()
        widgets_page.scroll_to_avoid_footer(widgets_page.browser.find_element(*accordian_service_loc.PROGRESS_BAR_ITEM))
        widgets_page.browser.find_element(*accordian_service_loc.PROGRESS_BAR_ITEM).click()
        
        widgets_page.browser.find_element(*progressbar_loc.START_STOP_BUTTON).click()
        widgets_page.wait.until(EC.text_to_be_present_in_element((progressbar_loc.PROGRESS_BAR_LINE),progressbar_loc.expected_progress))
        widgets_page.browser.find_element(*progressbar_loc.START_STOP_BUTTON).click()

        actual_progress = widgets_page.browser.find_element(*progressbar_loc.PROGRESS_BAR_LINE).text
        assert actual_progress == progressbar_loc.expected_progress, widgets_page.error_message('progress value', actual_progress, progressbar_loc.expected_progress)


    '''
Кейс №35: Индикатор — перезагрузка
    1. Открыть категорию Widgets, перейти в Progress Bar
    2. Нажать кнопку Start
    3. Дождаться заполнения индикатора до 100%
    4. Нажать кнопку Reset
    5. Проверить изменение индикатора
    '''
    def test_35_progress_bar_reset(self, browser):
        widgets_page = WidgetsPage(browser, wait_timeout=11)
        widgets_page.open()
        widgets_page.scroll_to_avoid_footer(widgets_page.browser.find_element(*accordian_service_loc.PROGRESS_BAR_ITEM))
        widgets_page.browser.find_element(*accordian_service_loc.PROGRESS_BAR_ITEM).click()
        
        widgets_page.browser.find_element(*progressbar_loc.START_STOP_BUTTON).click()
        widgets_page.wait.until(EC.text_to_be_present_in_element((progressbar_loc.PROGRESS_BAR_LINE), '100%'))
        #EC doesnt work here, only sleep has helped 
        time.sleep(0.5)
        widgets_page.browser.find_element(*progressbar_loc.RESET_BUTTON).click()
        
        # widgets_page.wait.until(EC.text_to_be_present_in_element_attribute((progressbar_loc.PROGRESS_BAR_LINE),'aria-valuenow','0'))

        actual_progress = widgets_page.browser.find_element(*progressbar_loc.PROGRESS_BAR_LINE).text
        assert actual_progress == '0%', widgets_page.error_message('progress value', actual_progress, '0%')


    '''
Кейс №36: Вкладки
    1. Открыть категорию Widgets, перейти в Tabs
    2. Поочередно открывать каждый блок вкладок и проверять наличие нужного текста и отсутствие ненужного

    > p.s. в 1 блоке не должно быть текста 2 и 3, во 2 нет текста 1 и 3, в 3 нет текста 1 и 2
    '''
    def test_36_tabs(self, browser):
        widgets_page = WidgetsPage(browser)
        widgets_page.open()
        widgets_page.scroll_to_avoid_footer(widgets_page.browser.find_element(*accordian_service_loc.TABS_ITEM))
        widgets_page.browser.find_element(*accordian_service_loc.TABS_ITEM).click()
        
        what_tab_text = widgets_page.get_tab_text(*tabs_loc.WHAT_TAB, *tabs_loc.WHAT_TAB_TEXT)
        origin_tab_text = widgets_page.get_tab_text(*tabs_loc.ORIGIN_TAB, *tabs_loc.ORIGIN_TAB_TEXTS)
        use_tab_text = widgets_page.get_tab_text(*tabs_loc.USE_TAB, *tabs_loc.USE_TAB_TEXT)

        assert origin_tab_text == tabs_loc.origin_tab_text, 'Wrong origin tab text'
        assert not what_tab_text in origin_tab_text, 'Origin tab has what tab text in it'
        assert not use_tab_text in origin_tab_text, 'Origin tab has use tab text in it'

        assert use_tab_text == tabs_loc.use_tab_text, 'Wrong use tab text'
        assert not what_tab_text in use_tab_text, 'Use tab has what tab text in it'
        assert not origin_tab_text in use_tab_text, 'User tab has origin tab text in it'

        assert what_tab_text == tabs_loc.what_tab_text, 'Wrong what tab text'
        assert not origin_tab_text in what_tab_text, 'What tab has origin tab text in it'
        assert not use_tab_text in what_tab_text, 'What tab has use tab text in it'

    '''
Кейс №37: Наведение
    1. Открыть категорию Widgets, перейти в Tool Tips
    2. Навести мышку на слово «Contrary» из блока с текстом
    3. Проверить наличие текста «You hovered over the Contrary» в появившемся блоке под словом
    '''
    @pytest.mark.current
    def test_37_tooltips(self, browser):
        widgets_page = WidgetsPage(browser)
        widgets_page.open()
        widgets_page.scroll_to_avoid_footer(widgets_page.browser.find_element(*accordian_service_loc.TOOL_TIPS_ITEM))
        widgets_page.browser.find_element(*accordian_service_loc.TOOL_TIPS_ITEM).click()

        contrary = widgets_page.browser.find_element(*tooltips_loc.CONTRARY_LINK_TO_HOVER)
        widgets_page.actions\
            .move_to_element(contrary)\
            .perform()

        # if not widgets_page.browser.find_element(*tooltips_loc.TOOL_TIP).is_displayed():
        widgets_page.wait.until(EC.visibility_of_element_located((tooltips_loc.TOOL_TIP)))
        actual_tooltip_text = widgets_page.browser.find_element(*tooltips_loc.TOOL_TIP).text
        assert actual_tooltip_text == tooltips_loc.expected_tooltip_text, \
            widgets_page.error_message('tool tip', actual_tooltip_text, tooltips_loc.expected_tooltip_text)


    '''
Кейс №38: Меню
    1. Открыть категорию Widgets, перейти в Menu
    2. Поочередно навести мышку на меню: «Main Item 2» => «SUB SUB LIST» => «Sub Sub Item 2»
    3. Проверить видимость объектов, с которыми происходили взаимодействия
    '''
    def test_38_menu(self, browser):
        widgets_page = WidgetsPage(browser)
        widgets_page.open()
        widgets_page.scroll_to_avoid_footer(widgets_page.browser.find_element(*accordian_service_loc.MENU_ITEM))
        widgets_page.browser.find_element(*accordian_service_loc.MENU_ITEM).click()

        main_item_2 = widgets_page.browser.find_element(*menu_loc.MAIN_ITEM_2)
        sub_sub_list = widgets_page.browser.find_element(*menu_loc.SUB_SUB_LIST)
        sub_sub_item_2 = widgets_page.browser.find_element(*menu_loc.SUB_SUB_ITEM_2)

        widgets_page.actions.move_to_element(main_item_2).move_to_element(sub_sub_list).move_to_element(sub_sub_item_2).perform()

        assert main_item_2.is_displayed(), 'Main item 2 is not visible'
        assert sub_sub_list.is_displayed(), 'SUB SUB LIST is not visible'
        assert sub_sub_item_2.is_displayed(), 'Sub sub item 2 is not visible'

        

        

