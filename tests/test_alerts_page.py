
import pytest
from locators import accordian_service_loc
from locators.alerts_frame_and_windows_locators import alerts_loc
from pages.alerts_frame_windows_page import AlertsFrameWindowsPage
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support import expected_conditions as EC


class TestAlerts:
    
    '''
Кейс №22: Диалоговое окно
    1. Открыть категорию Alerts, Frame & Windows, перейти в Alerts
    2. Нажать кнопку «Click Button to see alert»
    3. Проверить текст в диалоговом окне
    4. Нажать «ОК» в диалоговом окне
    5. Проверить результат
    '''
    def test_22_alert(self, browser):
        afw_page = AlertsFrameWindowsPage(browser)
        afw_page.open()
        afw_page.browser.find_element(*accordian_service_loc.ALERTS_ITEM).click()

        afw_page.browser.find_element(*alerts_loc.ALERT_BUTTON).click()
        actual_alert_text = Alert(afw_page.browser).text
        assert actual_alert_text == alerts_loc.expected_alert_text, afw_page.error_message('alert text', actual_alert_text, alerts_loc.expected_alert_text)

        Alert(afw_page.browser).accept()
        assert afw_page.alert_is_disappeared(), 'Alert is still visible'


    '''
Кейс №23: Диалоговое окно с задержкой
    1. Открыть категорию Alerts, Frame & Windows, перейти в Alerts
    2. Нажать кнопку «On button click, alert will appear after 5 seconds»
    3. Дождаться появления диалогового окна
    4. Проверить текст в диалоговом окне
    5. Нажать «ОК» в диалоговом окне
    6. Проверить результат
    '''    
    def test_23_delayed_alert(self, browser):
        afw_page = AlertsFrameWindowsPage(browser, wait_timeout=5.5)
        afw_page.open()
        afw_page.browser.find_element(*accordian_service_loc.ALERTS_ITEM).click()

        afw_page.browser.find_element(*alerts_loc.DELAYED_ALERT_BUTTON).click()
        afw_page.wait.until(EC.alert_is_present())
        actual_alert_text = Alert(afw_page.browser).text
        assert actual_alert_text == alerts_loc.expected_delayed_alert_text, afw_page.error_message('delayed alert text', actual_alert_text, alerts_loc.expected_delayed_alert_text)

        Alert(afw_page.browser).accept()
        assert afw_page.alert_is_disappeared(), 'Delayed alert is still visible'


    '''
Кейс №24: Диалоговое окно с несколькими кнопками
    1. Открыть категорию Alerts, Frame & Windows, перейти в Alerts
    2. Нажать кнопку «Click Button to see alert»
    3. Проверить текст в диалоговом окне
    4. Нажать «Cancel» в диалоговом окне
    5. Проверить результат
    '''
    def test_24_dialog_alert(self, browser):
        afw_page = AlertsFrameWindowsPage(browser)
        afw_page.open()
        afw_page.browser.find_element(*accordian_service_loc.ALERTS_ITEM).click()

        afw_page.browser.find_element(*alerts_loc.DIALOG_ALERT_BUTTON).click()
        curr_text = Alert(afw_page.browser).text
        assert curr_text == alerts_loc.expected_dialog_alert_text, \
            afw_page.error_message('alert text', curr_text, alerts_loc.expected_dialog_alert_text)
        
        Alert(browser).dismiss()
        assert afw_page.alert_is_disappeared(), 'Alert is still visible'
        
        actual_result = afw_page.browser.find_element(*alerts_loc.DIALOG_ALERT_RESULT_TEXT).text
        expected_result_word = alerts_loc.expected_dialog_alert_result
        assert expected_result_word in actual_result, f'No "{expected_result_word}" in alert result'\
            '"{actual_result}"'


    '''
Кейс №25: Диалоговое окно с вводом данных
    1. Открыть категорию Alerts, Frame & Windows, перейти в Alerts
    2. Нажать кнопку «On button click, prompt box will appear»
    3. Проверить текст в диалоговом окне
    4. В поле ввода текста ввести своё имя и нажать «ОК»
    5. Проверить результат
    '''
    def test_25_prompt_alert(self, browser):
        afw_page = AlertsFrameWindowsPage(browser)
        afw_page.open()
        afw_page.browser.find_element(*accordian_service_loc.ALERTS_ITEM).click()

        afw_page.browser.find_element(*alerts_loc.PROMPT_ALERT_BUTTON).click()
        alert = Alert(afw_page.browser)
        actual_alert_text = alert.text
        assert actual_alert_text == alerts_loc.expected_prompt_alert_text, afw_page.error_message('alert text', actual_alert_text, alerts_loc.expected_prompt_alert_text)

        alert.send_keys(alerts_loc.prompt_text_to_enter)
        alert.accept()
        assert afw_page.alert_is_disappeared(), 'Prompt alert is still visible'
        
        actual_result = afw_page.browser.find_element(*alerts_loc.PROMPT_ALERT_RESULT).text
        expected_result_word = alerts_loc.prompt_text_to_enter
        assert expected_result_word in actual_result, f'No "{expected_result_word}" in alert_result "{actual_result}"'




