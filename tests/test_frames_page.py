

import time

import pytest
from locators import accordian_service_loc
from locators.alerts_frame_and_windows_locators import frames_loc
from pages.alerts_frame_windows_page import AlertsFrameWindowsPage


class TestFramesPage:
    
    '''
Кейс №26: Страница в странице
    1. Открыть категорию Alerts, Frame & Windows, перейти в Frames
    2. Определить текст, который находится в большом фрейме (id="frame1")
    3. Проверить полученный текст на соответствие «This is a sample page»
    '''
    def test_26_text_in_big_frame(self, browser):
        afw_page = AlertsFrameWindowsPage(browser)
        afw_page.open()
        afw_page.browser.find_element(*accordian_service_loc.FRAMES_ITEM).click()

        afw_page.browser.switch_to.frame(afw_page.browser.find_element(*frames_loc.BIG_FRAME))
        
        actual_big_frame_text = afw_page.browser.find_element(*frames_loc.BIG_FRAME_TEXT).text
        assert actual_big_frame_text == frames_loc.expected_big_frame_text, afw_page.error_message('big frame text', actual_big_frame_text, frames_loc.expected_big_frame_text)

    '''
Кейс №27: Маленькая страница в странице
    1. Открыть категорию Alerts, Frame & Windows, перейти в Frames
    2. Изменить координат в маленьком фрейме (id="frame2") на 1000 по осям X и Y
    '''
    @pytest.mark.current
    def test_27_scroll_in_small_frame(self, browser):
        afw_page = AlertsFrameWindowsPage(browser)
        afw_page.open()
        afw_page.browser.find_element(*accordian_service_loc.FRAMES_ITEM).click()

        afw_page.browser.switch_to.frame(afw_page.browser.find_element(*frames_loc.SMALL_FRAME))
        afw_page.browser.execute_script('window.scrollTo(1000, 1000);')
        time.sleep(2)
