

import pytest
from locators import accordian_service_loc
from locators.interactions_locators import (dragable_loc, dropable_loc,
                                            selectable_loc, sortable_loc)
from pages.Interactions_page import InterationsPage
from selenium.webdriver.common.by import By


class TestInteractionsPage:
    
    '''
Кейс №39: Сортировка списка
    1. Открыть категорию Interactions, перейти в Sortable
    2. Во вкладке List провести сортировку списка (от верхнего к нижнему): Six Four Two Five Three One (6 4 2 5 3 1)
    3. Проверить полученный список
    '''
    def test_39_sortable(self, browser):
        interactions_page = InterationsPage(browser)
        interactions_page.open()
        interactions_page.browser.find_element(*accordian_service_loc.SORTABLE_ITEM).click()
        
        for title in sortable_loc.expected_title_list:
            list_to_sort = interactions_page.browser.find_elements(*sortable_loc.SORTABLE_LIST_ITEMS)
            item_to_move = interactions_page.browser.find_element(By.XPATH,\
                f"//*[contains(@class, 'vertical-list-container')]//div[text() = '{title}']")
            position_move_to = sortable_loc.expected_title_list.index(title)
            
            current_position = list_to_sort.index(item_to_move)
            if current_position == position_move_to:
                pass
            else:
                offset = (position_move_to - current_position)  * 50
                interactions_page.actions\
                    .click_and_hold(item_to_move)\
                    .move_by_offset(0, offset)\
                    .release()\
                    .perform()

        actual_title_list = interactions_page.get_titles_list_located(*sortable_loc.SORTABLE_LIST_ITEMS)
        assert actual_title_list == sortable_loc.expected_title_list,\
            interactions_page.error_message('item order', actual_title_list, sortable_loc.expected_title_list)


    '''
Кейс №40: Выбор строк
    1. Открыть категорию Interactions, перейти в Selectable
    2. Во вкладке List выбрать от 1 до 3 строк
    3. Проверить выделение этих строк
    '''
    def test_40_selectable(self, browser):
        interactions_page = InterationsPage(browser)
        interactions_page.open()
        interactions_page.browser.find_element(*accordian_service_loc.SELECTABLE_ITEM).click()

        all_rows = interactions_page.browser.find_elements(*selectable_loc.ROWS)
        all_rows[0].click()
        all_rows[2].click()
        all_rows[1].click()
        
        actual_active_rows = interactions_page.get_titles_list_located(*selectable_loc.ACTIVE_ROWS)
        assert actual_active_rows == selectable_loc.expected_active_row_titles,\
            interactions_page.error_message('active row titles',\
            actual_active_rows, selectable_loc.expected_active_row_titles)


    '''
Кейс №41: Перетаскивание в блоки
    1. Открыть категорию Interactions, перейти в Droppable
    2. Перейти во вкладку Prevent Propogation
    3. Перетащить блок Drag Me во внутренний блок Inner droppable (greedy) нижнего большого блока
    4. Проверить выделение блока
    '''
    def test_41_droppable(self, browser):
        interactions_page = InterationsPage(browser)
        interactions_page.open()
        interactions_page.scroll_to_avoid_footer(interactions_page.browser.find_element(*accordian_service_loc.DROPPABLE_ITEM))
        interactions_page.browser.find_element(*accordian_service_loc.DROPPABLE_ITEM).click()

        interactions_page.browser.find_element(*dropable_loc.PREVENT_PROPOGATION_TAB).click()
        draggable_item = interactions_page.browser.find_element(*dropable_loc.DRAGGABLE_ITEM)
        greedy_droppable_item = interactions_page.browser.find_element(*dropable_loc.GREEDY_DROPPABLE_ITEM)
        interactions_page.actions\
            .click_and_hold(draggable_item)\
            .move_to_element(greedy_droppable_item)\
            .release()\
            .perform()
        
        actual_droppable_color = greedy_droppable_item.value_of_css_property('background-color')
        assert actual_droppable_color == dropable_loc.expected_droppable_color, interactions_page.error_message('droppable color', actual_droppable_color, dropable_loc.expected_droppable_color)


    '''
Кейс №42: Перетаскивание внутри блоков
    1. Открыть категорию Interactions, перейти в Dragabble
    2. Перейти во вкладку Container Restricted
    3. Переместить блок «I'm contained within the box» в правую нижнюю часть его родительского блока (большой верхний блок, его id="containmentWrapper")
    4. Проверить перемещение блока «I'm contained within the box»
    '''
    def test_42_dragable(self, browser):
        interactions_page = InterationsPage(browser)
        interactions_page.open()
        interactions_page.scroll_to_avoid_footer(interactions_page.browser.find_element(*accordian_service_loc.DRAGABLE_ITEM))
        interactions_page.browser.find_element(*accordian_service_loc.DRAGABLE_ITEM).click()
        

        interactions_page.browser.find_element(*dragable_loc.CONTAINER_RESTRICTED_TAB).click()
        item_to_drag = interactions_page.browser.find_element(*dragable_loc.ITEM_TO_DRAG) 
        interactions_page.actions\
            .click_and_hold(item_to_drag)\
            .move_by_offset(640, 106)\
            .release()\
            .perform()
        
        actual_x_offset = item_to_drag.value_of_css_property('left')
        assert actual_x_offset == dragable_loc.expected_x_offset,\
            interactions_page.error_message('x offset', actual_x_offset, dragable_loc.expected_x_offset)

        actual_y_offset = item_to_drag.value_of_css_property('top')
        assert actual_y_offset == dragable_loc.expected_y_offset,\
            interactions_page.error_message('y offset', actual_y_offset, dragable_loc.expected_y_offset)





