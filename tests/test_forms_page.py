

import pytest
from locators import accordian_service_loc
from locators.forms_locators import practice_form_loc as _
from pages.forms_page import FormsPage
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC


class TestFormsPage():
    
    '''
Кейс №19: Валидные данные в форме
    1. Открыть категорию Forms, перейти в Practice Form
    2. Заполнить все поля валидными данными (файл загружать не обязательно)
    3. Проверить правильность введенных данных во всплывающем модальном окне
    '''
    def test_19_correct_data(self, browser):
        forms_page = FormsPage(browser)
        forms_page.open()
        forms_page.browser.find_element(*accordian_service_loc.PRACTICE_FORM_ITEM).click()

        forms_page.browser.find_element(*_.FIRST_NAME_INPUT).send_keys(_.first_name)
        forms_page.browser.find_element(*_.LAST_NAME_INPUT).send_keys(_.last_name)
        forms_page.browser.find_element(*_.EMAIL_INPUT).send_keys(_.email_valid)
        forms_page.browser.find_element(*_.MALE_RADIOBUTTON).click()
        forms_page.browser.find_element(*_.MOBILE_NUMBER_INPUT).send_keys(_.mobile_number_valid)
        forms_page.browser.find_element(*_.DATE_OF_BIRTH_INPUT).click()
        forms_page.set_date(_.birth_date_to_set)

        subjects_input = forms_page.browser.find_element(*_.SUBJECTS_INPUT)
        subjects_input.click()
        subjects_input.send_keys(_.search_key_for_subjects)
        for integer in range(2):
            subjects_input.send_keys(Keys.ARROW_DOWN)
        subjects_input.send_keys(Keys.ENTER)
        
        forms_page.browser.find_element(*_.SPORTS_CHECKBOX).click()
        forms_page.browser.find_element(*_.CURRENT_ADDRESS_INPUT).send_keys(_.current_address)
        forms_page.browser.find_element(*_.STATE_SELECT).click()
        forms_page.browser.find_element(*_.NCR_STATE_ITEM).click()
        forms_page.browser.find_element(*_.CITY_SELECT).click()
        forms_page.browser.find_element(*_.FIRST_CITY_ITEM).click()

        forms_page.browser.find_element(*_.SUBMIT_BUTTON).click()
        
        actual_results = forms_page.get_results()
        assert actual_results == _.expected_results_full_form, forms_page.error_message('results in form', actual_results, _.expected_results_full_form)


    '''
Кейс №20: Валидные данные в обязательных полях
    1. Открыть категорию Forms, перейти в Practice Form
    2. Заполнить обязательные поля в форме
    3. Проверить правильность введенных данных во всплывающем модальном окне 
    '''
    def test_20_only_required_inputs(self, browser):
        forms_page = FormsPage(browser)
        forms_page.open()
        forms_page.browser.find_element(*accordian_service_loc.PRACTICE_FORM_ITEM).click()

        forms_page.browser.find_element(*_.FIRST_NAME_INPUT).send_keys(_.first_name)
        forms_page.browser.find_element(*_.LAST_NAME_INPUT).send_keys(_.last_name)
        forms_page.browser.find_element(*_.MALE_RADIOBUTTON).click()
        forms_page.browser.find_element(*_.MOBILE_NUMBER_INPUT).send_keys(_.mobile_number_valid)
        forms_page.browser.find_element(*_.DATE_OF_BIRTH_INPUT).click()
        forms_page.set_date(_.birth_date_to_set)
        forms_page.browser.find_element(*_.SUBMIT_BUTTON).click()

        actual_results = forms_page.get_results()
        assert actual_results == _.expected_results_required_only, forms_page.error_message('results in form', actual_results, _.expected_results_required_only)


    '''
Кейс №21: Невалидные данные в форме
    1. Открыть категорию Forms, перейти в Practice Form
    2. Заполнить все поля невалидными данными (файл загружать не обязательно)
    3. Проверить выделение полей которые были неправильно заполнены
    '''
    @pytest.mark.parametrize('first_name, last_name, email, select_radio, mobile_number, expected_errors_quantity', _.input_parameters)
    def test_21_invalid_input(self, browser, first_name, last_name, email, select_radio, mobile_number, expected_errors_quantity):
        forms_page = FormsPage(browser)
        forms_page.open()
        forms_page.browser.find_element(*accordian_service_loc.PRACTICE_FORM_ITEM).click()

        first_name_input = forms_page.browser.find_element(*_.FIRST_NAME_INPUT)
        last_name_input = forms_page.browser.find_element(*_.LAST_NAME_INPUT)
        email_input = forms_page.browser.find_element(*_.EMAIL_INPUT)
        male_radiobutton = forms_page.browser.find_element(*_.MALE_RADIOBUTTON)
        mobile_number_input = forms_page.browser.find_element(*_.MOBILE_NUMBER_INPUT)

        first_name_input.send_keys(first_name)
        last_name_input.send_keys(last_name)
        email_input.send_keys(email)
        if select_radio:
            male_radiobutton.click()
        mobile_number_input.send_keys(mobile_number)
        forms_page.browser.find_element(*_.SUBMIT_BUTTON).click()

        input_list = [first_name_input, last_name_input, email_input, male_radiobutton, mobile_number_input]
        actual_errors_quantity = 0
        
        for element in input_list:
            if forms_page.element_is_invalid(element, _.invalid_input_color):
                actual_errors_quantity +=1
            else:
                pass

        assert actual_errors_quantity == expected_errors_quantity, forms_page.error_message('number of inputs with errors', actual_errors_quantity, expected_errors_quantity)
        

