

import pytest
from locators import mainpage_loc
from pages.main_page import MainPage


class TestMainPage:


    '''
Кейс №1: Логотип на главной
    1. Перейти на главную страницу
    2. Проверить наличие шести категорий: Elements, Forms, Alerts, Widgets, Interactions, Book
    3. Проверить переход с лого в шапке на главную страницу

Кейс №2: Переход в категорию
    1. Перейти на главную страницу
    2. Проверить переход по любой из категорий

Кейс №3: Логотип в категориях
    1. Перейти на главную страницу
    2. Перейти по любой категории
    3. Перейти на главную страницу по лого в шапке
    '''
    def test_01_02_03_main_logo_and_categories_links(self, browser):
        main_page = MainPage(browser)
        main_page.open()
        
        counter = len(main_page.browser.find_elements(*mainpage_loc.CATEGORY_ELEMENTS))
        
        for i in range(counter):
            category_elements = main_page.browser.find_elements(*mainpage_loc.CATEGORY_ELEMENTS)
            current_category = category_elements[i]
            
            main_page.scroll_to_avoid_footer(current_category)
            current_category.click()
            main_page.check_current_url(mainpage_loc.expected_category_links[i])

            main_logo = main_page.browser.find_element(*mainpage_loc.MAIN_LOGO)
            main_logo.click()
            main_page.check_current_url(mainpage_loc.LINK)









    
        