

import os
import sys
from datetime import datetime

import pytest
from genericpath import isdir
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromService
from webdriver_manager.chrome import ChromeDriverManager


@pytest.fixture(scope='session', autouse=True)
def dir_for_screenshots():
    path = f'{os.path.curdir}/screenshots/{datetime.now().strftime("%Y-%m-%d_%H:%M:%S")}'
    os.makedirs(path)
    yield path


@pytest.fixture(scope = 'function')
def browser():
    options = webdriver.ChromeOptions()
    options.add_argument('window-size=1920,1080')
    options.add_argument('incognito')
    options.add_argument('headless')
    browser = webdriver.Chrome(service=ChromService(ChromeDriverManager().install()), options=options)
    yield browser


@pytest.hookimpl(tryfirst=True, hookwrapper=True)
def pytest_runtest_makereport(item, call):
    # execute all other hooks to obtain the report object
    outcome = yield
    rep = outcome.get_result()

    # set a report attribute for each phase of a call, which can
    # be "setup", "call", "teardown"

    setattr(item, "rep_" + rep.when, rep)


@pytest.fixture(scope="function", autouse=True)
def test_failed_check(request, dir_for_screenshots):
    yield
    # request.node is an "item" because we use the default
    # "function" scope
    if request.node.rep_call.failed:
        result = 'fail'
    elif request.node.rep_call.passed:
        result = 'pass'
    # create directory pass/fail if it doesnt exist
    if not os.path.isdir(f'{dir_for_screenshots}/{result}'):
        os.makedirs(f'{dir_for_screenshots}/{result}')

    test_name = os.environ.get('PYTEST_CURRENT_TEST').split(':')[-1].split(' ')[0]
    file_name = f'{dir_for_screenshots}/{result}/{test_name}_{datetime.now().strftime("%Y-%m-%d_%H:%M")}.png'
    
    browser = request.node.funcargs['browser']
    browser.save_screenshot(file_name)
    browser.quit()
    



    
    

