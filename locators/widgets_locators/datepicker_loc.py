from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/date-picker'

DATE_AND_TIME_INPUT = (By.CSS_SELECTOR, '#dateAndTimePickerInput')
MONTH_SELECT = (By.CSS_SELECTOR, '.react-datepicker__month-dropdown-container')
MONTHS_LIST = (By.CSS_SELECTOR, '.react-datepicker__month-option')
YEAR_SELECT = (By.CSS_SELECTOR, '.react-datepicker__year-dropdown-container')
YEARS_LIST = (By.CSS_SELECTOR, '.react-datepicker__year-option')
DATE_AND_TIME_INPUT_TEXT = (By.CSS_SELECTOR, '#dateAndTimePickerInput')

expected_date_and_time = 'November 27, 1940 12:00 PM'

