from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/tool-tips'

CONTRARY_LINK_TO_HOVER = (By.CSS_SELECTOR, '#texToolTopContainer a:first-child')
TOOL_TIP = (By.CSS_SELECTOR, '.tooltip-inner')

expected_tooltip_text = 'You hovered over the Contrary'
