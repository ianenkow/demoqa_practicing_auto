from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/progress-bar'

START_STOP_BUTTON = (By.CSS_SELECTOR, '#startStopButton')
RESET_BUTTON = (By.CSS_SELECTOR, '#resetButton')
PROGRESS_BAR_LINE = (By.CSS_SELECTOR, '#progressBar .progress-bar')

expected_progress = '20%'
