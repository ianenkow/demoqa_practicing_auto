from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/slider'

SLIDER_VALUE_BOX = (By.CSS_SELECTOR, '#sliderValue')
SLIDER = (By.CSS_SELECTOR, '.range-slider')


expected_slider_value = 72
