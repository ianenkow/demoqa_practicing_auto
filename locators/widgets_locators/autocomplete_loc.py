from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/auto-complete'

MULTIPLE_COLORS_INPUT = (By.CSS_SELECTOR, '#autoCompleteMultipleInput')
MULTIPLE_COLORS_INPUT_CHOSEN_COLORS = (By.CSS_SELECTOR, '.auto-complete__multi-value__label')
SINGLE_COLOR_INPUT = (By.CSS_SELECTOR, '#autoCompleteSingleInput')
SINGLE_COLOR_INPUT_CHOSEN_COLOR = (By.CSS_SELECTOR, '.auto-complete__single-value')
INPUT_OPTIONS = (By.CSS_SELECTOR, '.auto-complete__option')



expected_colors = ['Red', 'Purple', 'Aqua']
expected_color = 'Indigo'
not_expected_color = 'Blue'
