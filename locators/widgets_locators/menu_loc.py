from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/menu#'

MAIN_ITEM_2 = (By.XPATH, '//*[@id="nav"]//*[text()="Main Item 2"]')
SUB_SUB_LIST = (By.XPATH, '//*[@id="nav"]//*[text()="SUB SUB LIST »"]')
SUB_SUB_ITEM_2 = (By.XPATH, '//*[@id="nav"]//*[text()="Sub Sub Item 2"]')
