from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/dynamic-properties'

DELAYED_CLICKABILITY_BUTTON = (By.CSS_SELECTOR, '#enableAfter')
COLOR_CHANGE_BUTTON = (By.CSS_SELECTOR, '#colorChange')
DELAYED_VISIBILITY_BUTTON = (By.CSS_SELECTOR, '#visibleAfter')

expected_color = 'rgba(220, 53, 69, 1)'
