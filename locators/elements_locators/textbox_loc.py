from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/text-box'

FULL_NAME_INPUT = (By.CSS_SELECTOR, '#userName')
EMAIL_INPUT = (By.CSS_SELECTOR, '#userEmail')
CURRENT_ADDRESS_INPUT = (By.CSS_SELECTOR, '#currentAddress.form-control')
PERMANENT_ADDRESS_INPUT = (By.CSS_SELECTOR, '#permanentAddress.form-control')
SUBMIT_BUTTON = (By.CSS_SELECTOR, '#submit')
NAME_LOCATOR = (By.CSS_SELECTOR, '#name')
EMAIL_LOCATOR = (By.CSS_SELECTOR, '#email')
CURRENT_ADDRESS_LOCATOR = (By.CSS_SELECTOR, '#currentAddress.mb-1')
PERMANENT_ADDRESS_LOCATOR = (By.CSS_SELECTOR, '#permanentAddress.mb-1')

email = 'chubaka@gov.ru'
current_address = 'Турция, Стамбул'
permanent_address = 'Россия, Москва, Кремль'
invalid_email = 'ya@ru'
full_name = 'Чубайс Анатолий Борисович'
error_attribute = 'class'
error_attribute_value = 'field-error'
