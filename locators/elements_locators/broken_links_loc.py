from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/broken'

VALID_IMAGE = (By.CSS_SELECTOR, 'div.col-md-6 img:nth-of-type(1)')
BROKEN_IMAGE = (By.CSS_SELECTOR, 'div.col-md-6 img:nth-of-type(2)')
VALID_LINK = (By.CSS_SELECTOR, 'div.col-md-6 a:nth-of-type(1)')
BROKEN_LINK = (By.CSS_SELECTOR, 'div.col-md-6 a:nth-of-type(2)')
MESSAGE_WITH_STATUS = (By.CSS_SELECTOR, '.example > p')

status_to_check = '500'

