from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/buttons'

DOUBLE_CLICK_BUTTON = (By.CSS_SELECTOR, '#doubleClickBtn')
RIGHT_CLICK_BUTTON = (By.CSS_SELECTOR, '#rightClickBtn')
CLICK_ME_BUTTON = (By.XPATH, '//button[text() = "Click Me"]')
DOUBLE_CLICK_MESSAGE = (By.CSS_SELECTOR, '#doubleClickMessage')
RIGHT_CLICK_MESSAGE = (By.CSS_SELECTOR, '#rightClickMessage')
CLICK_MESSAGE = (By.CSS_SELECTOR, '#dynamicClickMessage')

double_click_message = 'You have done a double click'
right_click_message = 'You have done a right click'
click_message = 'You have done a dynamic click'

