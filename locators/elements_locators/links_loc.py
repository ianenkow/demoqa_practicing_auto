from http.client import (BAD_REQUEST, FORBIDDEN, NO_CONTENT, NOT_FOUND,
                         UNAUTHORIZED)

from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/links'

HOME_LINK = (By.CSS_SELECTOR, '#simpleLink')
RESPONSE_STATUS_AND_TEXT = (By.CSS_SELECTOR, '#linkResponse b')

CREATED_LINK = (By.CSS_SELECTOR, '#created')
NO_CONTENT_LINK = (By.CSS_SELECTOR, '#no-content')
MOVED_LINK = (By.CSS_SELECTOR, '#moved')
BAD_REQUEST_LINK = (By.CSS_SELECTOR, '#bad-request')
UNAUTHORIZED_LINK = (By.CSS_SELECTOR, '#unauthorized')
FORBIDDEN_LINK = (By.CSS_SELECTOR, '#forbidden')
NOT_FOUND_LINK = (By.CSS_SELECTOR, '#invalid-url')

links_to_click = [CREATED_LINK, NO_CONTENT_LINK, MOVED_LINK, BAD_REQUEST_LINK, UNAUTHORIZED_LINK, FORBIDDEN_LINK, NOT_FOUND_LINK]
expected_statuses = ['201', '204', '301', '400', '401', '403', '404']
expected_texts = ['Created', 'No Content', 'Moved Permanently', 'Bad Request', 'Unauthorized', 'Forbidden', 'Not Found']





