from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/checkbox'

EXPAND_ARROWS = (By.CSS_SELECTOR, '.rct-collapse-btn')
CHECKBOX_TITLES = (By.CSS_SELECTOR, '.rct-title')
SELECTED_TITLES = (By.CSS_SELECTOR, '.text-success')

checkbox_titles_to_select = ['Notes', 'Angular', 'General', 'Word File.doc']
expected_titles = ['notes', 'angular', 'general', 'wordFile']
