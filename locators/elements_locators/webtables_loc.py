from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/webtables'


ADD_BUTTON = (By.CSS_SELECTOR, '#addNewRecordButton')
FIRST_NAME_INPUT = (By.CSS_SELECTOR, '#firstName')
LAST_NAME_INPUT = (By.CSS_SELECTOR, '#lastName')
EMAIL_INPUT = (By.CSS_SELECTOR, '#userEmail')
AGE_INPUT = (By.CSS_SELECTOR, '#age')
SALARY_INPUT = (By.CSS_SELECTOR, '#salary')
DEPARTMENT_INPUT = (By.CSS_SELECTOR, '#department')
SUBMIT_BUTTON = (By.CSS_SELECTOR, '#submit')
EDIT_BUTTON = (By.CSS_SELECTOR, '#edit-record-')
DELETE_SECOND_ROW_BUTTON = (By.CSS_SELECTOR, '#delete-record-2')
ROWS_PER_PAGE_SELECT = (By.CSS_SELECTOR, "[aria-label = 'rows per page']")
PAGE_NUMBER_INPUT = (By.CSS_SELECTOR, "input[type = 'number']")
NEXT_BUTTON = (By.XPATH, "//button[text() = 'Next']")

FIRST_ROW = (By.CSS_SELECTOR, '.rt-tr-group:nth-of-type(1) .rt-td')
SECOND_ROW = (By.CSS_SELECTOR, '.rt-tr-group:nth-of-type(2) .rt-td')
THIRD_ROW = (By.CSS_SELECTOR, '.rt-tr-group:nth-of-type(3) .rt-td')
FORTH_ROW = (By.CSS_SELECTOR, '.rt-tr-group:nth-of-type(4) .rt-td')
# it's very last row, not last filled
LAST_ROW = (By.CSS_SELECTOR, '.rt-tr-group:last-of-type .rt-td')
NOT_EMPTY_ROW = (By.CSS_SELECTOR, '.rt-tr:not(.-padRow)')



keys_for_row = ['first name', 'last name', 'age', 'email', 'salary', 'department']
row_to_add = {'first name': 'Джон', 'last name': 'Смит', 'age': '20', 'email': 'smith@kavichki.com', 'salary': '150000', 'department': 'Service'}
rows_to_add = [{'first name': 'Джон', 'last name': 'Смит', 'age': '20', 'email': 'smith@kavichki.com', 'salary': '150000', 'department': 'Service'},
    {'first name': 'Клиф', 'last name': 'Блежински', 'age': '40', 'email': 'cliffyB@amazon.com', 'salary': '550000', 'department': 'Game design'},
    {'first name': 'Shannen', 'last name': 'Doherty', 'age': '20', 'email': 'shado@holywood.com', 'salary': '123456', 'department': 'Charmed'}]

