from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/radio-button'

YES_RADIOBUTTON = (By.CSS_SELECTOR, '[for = yesRadio]')
IMPRESSIVE_RADIOBUTTON = (By.CSS_SELECTOR, '[for = impressiveRadio]')
NO_RADIOBUTTON = (By.CSS_SELECTOR, '[for = noRadio]')
SELECTED_RADIOBUTTON_TITLE = (By.CSS_SELECTOR, '.text-success')

disabled_attribute_to_check = 'class'
disabled_value = 'disabled'
