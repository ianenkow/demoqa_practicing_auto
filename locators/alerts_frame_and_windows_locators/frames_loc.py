from selenium.webdriver.common.by import By

FRAMES_LINK = 'https://demoqa.com/frames'
BIG_FRAME = (By.CSS_SELECTOR, '#frame1')
SMALL_FRAME = (By.CSS_SELECTOR, '#frame2')
BIG_FRAME_TEXT = (By.CSS_SELECTOR, '#sampleHeading')

expected_big_frame_text = 'This is a sample page'
