from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/alerts'

ALERT_BUTTON = (By.CSS_SELECTOR, '#alertButton')
DELAYED_ALERT_BUTTON = (By.CSS_SELECTOR, '#timerAlertButton')
DIALOG_ALERT_BUTTON = (By.CSS_SELECTOR, '#confirmButton')
DIALOG_ALERT_RESULT_TEXT = (By.CSS_SELECTOR, '#confirmResult')
PROMPT_ALERT_BUTTON = (By.CSS_SELECTOR, '#promtButton')
PROMPT_ALERT_RESULT = (By.CSS_SELECTOR, '#promptResult')


expected_alert_text = 'You clicked a button'
expected_delayed_alert_text = 'This alert appeared after 5 seconds'
expected_dialog_alert_text = 'Do you confirm action?'
expected_dialog_alert_result = 'Cancel'
expected_prompt_alert_text = 'Please enter your name'
prompt_text_to_enter = 'Ivan'
