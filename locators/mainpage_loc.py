from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/'

MAIN_LOGO = (By.CSS_SELECTOR, 'header img')
CATEGORY_ELEMENTS = (By.CSS_SELECTOR, '.top-card')

expected_category_titles = ['Elements', 'Forms', 'Alerts, Frame & Windows', 'Widgets', 'Interactions', 'Book Store Application']
expected_category_links = ['https://demoqa.com/elements', 'https://demoqa.com/forms', 'https://demoqa.com/alertsWindows', 'https://demoqa.com/widgets', 'https://demoqa.com/interaction', 'https://demoqa.com/books']


