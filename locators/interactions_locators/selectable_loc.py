from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/selectable'

ROWS = (By.CSS_SELECTOR, '#verticalListContainer .list-group-item')
ACTIVE_ROWS = (By.CSS_SELECTOR, '#verticalListContainer .list-group-item.active')

expected_active_row_titles = ['Cras justo odio', 'Dapibus ac facilisis in', 'Morbi leo risus']
