from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/droppable'

PREVENT_PROPOGATION_TAB = (By.CSS_SELECTOR, '#droppableExample-tab-preventPropogation')
DRAGGABLE_ITEM = (By.CSS_SELECTOR, '#dragBox')
GREEDY_DROPPABLE_ITEM = (By.CSS_SELECTOR, '#greedyDropBoxInner')


expected_droppable_color = 'rgba(70, 130, 180, 1)'
