from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/sortable'

SORTABLE_LIST_ITEMS = (By.CSS_SELECTOR, '.vertical-list-container .list-group-item-action')

expected_title_list = ['Six', 'Four', 'Two', 'Five', 'Three', 'One']
