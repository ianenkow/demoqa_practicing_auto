from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/dragabble'

CONTAINER_RESTRICTED_TAB = (By.CSS_SELECTOR, '#draggableExample-tab-containerRestriction')
ITEM_TO_DRAG = (By.CSS_SELECTOR, '#containmentWrapper .draggable')

expected_x_offset = '640px'
expected_y_offset = '106px'

