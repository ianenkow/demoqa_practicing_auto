from selenium.webdriver.common.by import By

ELEMENTS_LINK = 'https://demoqa.com/elements'
FORMS_LINK = 'https://demoqa.com/forms'
ALERTS_FRAME_WINDOWS_LINK = 'https://demoqa.com/alertsWindows'
WIDGETS_LINK = 'https://demoqa.com/widgets'
INTERACTIONS_LINK = 'https://demoqa.com/interaction'

#CATEGORY LEFT MENU
TEXTBOX_ITEM = (By.CSS_SELECTOR, '#item-0')
CHECKBOX_ITEM = (By.CSS_SELECTOR, '#item-1')
RADIOBUTTON_ITEM = (By.CSS_SELECTOR, '#item-2')
WEBTABLE_ITEM = (By.CSS_SELECTOR, '#item-3')
BUTTONS_ITEM = (By.CSS_SELECTOR, '#item-4')
LINKS_ITEM = (By.CSS_SELECTOR, '#item-5')
BROKEN_LINKS_ITEM = (By.CSS_SELECTOR, '#item-6')
DYNAMIC_PROPERTIES_ITEM = (By.CSS_SELECTOR, '#item-8')

PRACTICE_FORM_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-0')

ALERTS_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-1')
FRAMES_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-2')

# WIDGETS
ACCORDIAN_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-0')
AUTO_COMPLETE_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-1')
DATE_PICKER_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-2')
SLIDER_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-3')
PROGRESS_BAR_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-4')
TABS_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-5')
TOOL_TIPS_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-6')
MENU_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-7')

# INTERACTIONS
SORTABLE_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-0')
SELECTABLE_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-1')
DROPPABLE_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-3')
DRAGABLE_ITEM = (By.CSS_SELECTOR, '.element-list.show #item-4')
