from selenium.webdriver.common.by import By

LINK = 'https://demoqa.com/automation-practice-form'

FIRST_NAME_INPUT = (By.CSS_SELECTOR, '#firstName')
LAST_NAME_INPUT = (By.CSS_SELECTOR, '#lastName')
EMAIL_INPUT = (By.CSS_SELECTOR, '#userEmail')
MALE_RADIOBUTTON = (By.CSS_SELECTOR, '[for=gender-radio-1]')
FEMALE_RADIOBUTTON = (By.CSS_SELECTOR, '[for=gender-radio-2]')
OTHER_RADIOBUTTON = (By.CSS_SELECTOR, '[for=gender-radio-3]')
MOBILE_NUMBER_INPUT = (By.CSS_SELECTOR, '#userNumber')
DATE_OF_BIRTH_INPUT = (By.CSS_SELECTOR, '#dateOfBirthInput')
MONTH_SELECT = (By.CSS_SELECTOR, '.react-datepicker__month-select')
YEAR_SELECT = (By.CSS_SELECTOR, '.react-datepicker__year-select')
SUBJECTS_INPUT = (By.CSS_SELECTOR, '#subjectsInput')
SPORTS_CHECKBOX = (By.CSS_SELECTOR, '[for=hobbies-checkbox-1]')
READING_CHECKBOX = (By.CSS_SELECTOR, '[for=hobbies-checkbox-2]')
MUSIC_CHECKBOX = (By.CSS_SELECTOR, '[for=hobbies-checkbox-3]')
CURRENT_ADDRESS_INPUT = (By.CSS_SELECTOR, '#currentAddress')

STATE_SELECT = (By.CSS_SELECTOR, '#state')
NCR_STATE_ITEM = (By.CSS_SELECTOR, '#react-select-3-option-0')
UTTAR_PRADESH_STATE_ITEM = (By.CSS_SELECTOR, '#react-select-3-option-1')
HARYANA_STATE_ITEM = (By.CSS_SELECTOR, '#react-select-3-option-2')
RAJASTHAN_STATE_ITEM = (By.CSS_SELECTOR, '#react-select-3-option-3')

CITY_SELECT = (By.CSS_SELECTOR, '#city')
FIRST_CITY_ITEM = (By.CSS_SELECTOR, '#react-select-4-option-0')
SECOND_CITY_ITEM = (By.CSS_SELECTOR, '#react-select-4-option-1')
THIRD_CITY_ITEM = (By.CSS_SELECTOR, '#react-select-4-option-2')
SUBMIT_BUTTON = (By.CSS_SELECTOR, '#submit')
RESULT_WINDOW = (By.CSS_SELECTOR, '.modal-content')
RESULTS = (By.CSS_SELECTOR, 'tbody td:nth-of-type(2)')


first_name = 'Ivan'
last_name = 'Anenkov'
email_valid = 'ivan@anenkov.com'
gender = 'Male'
mobile_number_valid = '9876543210'
birth_date_to_set = '20.04.1988'
birth_date = '20 April,1988'
search_key_for_subjects = 'c'
subject = 'Computer Science'
current_address = 'Planet Earth'
state = 'NCR'
city = 'Delhi'
expected_results_full_form = [f'{first_name} {last_name}', email_valid, gender, mobile_number_valid, birth_date, subject, 'Sports', current_address, f'{state} {city}']
expected_results_required_only = [f'{first_name} {last_name}', gender, mobile_number_valid, birth_date]

email_invalid = 'ivan@com'
mobile_number_invalid = 'asdfwqertz'
invalid_input_color = 'rgb(220, 53, 69)'

input_parameters = [['', '', '@.ru', False, '', 5],
                    ['John', '', 'john@.ru', False, '', 4],
                    ['', 'Krasinsky', '@krasinsky.ru', False, '', 4],
                    ['', '', 'john@john.com', False, '', 4], 
                    ['', '', 'johnkrasinsky.ru', True, '', 4],
                    ['', '', '.ru', False, '1234567890', 4],
                    ['John', 'Krasinsky', 'john@john@john.ru', False, '', 3],
                    ['John', '', 'john@john.ru', False, '', 3],
                    ['John', '', '@@@', True, '', 3],
                    ['John', '', '...', False, '1234567890', 3],
                    ['', 'Krasinsky', 'john@john.ru', False, '', 3],
                    ['', 'Krasinsky', '@john@', True, '', 3],
                    ['', 'Krasinsky', 'ru.ru', False, '0123456789', 3]]




