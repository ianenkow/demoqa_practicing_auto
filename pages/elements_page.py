from locators import accordian_service_loc
from locators.elements_locators import checkbox_loc, webtables_loc
from locators.forms_locators import practice_form_loc
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select

from pages.base_page import BasePage


class ElementsPage(BasePage):

    def __init__(self, browser: webdriver.Chrome, timeout=5, wait_timeout=5):
        super().__init__(browser, accordian_service_loc.ELEMENTS_LINK, timeout, wait_timeout)


#CHECKBOXES       

    def select_checkboxes(self, titles_to_select):
        counter = 0
        for expected_title in titles_to_select:
            title_is_selected = False
            
            while not title_is_selected:
                expand_arrows = self.browser.find_elements(*checkbox_loc.EXPAND_ARROWS)
                enable_title_elements = self.browser.find_elements(*checkbox_loc.CHECKBOX_TITLES)
                
                for element in enable_title_elements:
                    if element.text == expected_title:
                        element.click()
                        title_is_selected = True
                        break   
                
                if title_is_selected:
                    break

                expand_arrows[counter].click()
                counter += 1


#WEBTABLES

    def add_row(self, row_to_add):
        self.browser.find_element(*webtables_loc.ADD_BUTTON).click()
        self.fill_add_row_form(row_to_add)
        self.browser.find_element(*webtables_loc.SUBMIT_BUTTON).click()


    def add_rows(self, rows_to_add):
        if type(rows_to_add) == dict:
            self.add_row(rows_to_add)
        elif type(rows_to_add) == list:
            for row in rows_to_add:
                self.add_row(row)
        else:
            print('Only dictionary or list of dictionaries allowed to add as row')


    def get_row(self, row_locator_type, row_locator):
        row_titles = self.get_titles_list_located(row_locator_type, row_locator)
        row = {}
        keys = webtables_loc.keys_for_row
        for integer in range(len(keys)):
            row[keys[integer]] = row_titles[integer]
        return row


    def edit_row(self, row_number, row_to_add):
        self.click_edit_button(row_number)
        self.clear_the_form()
        self.fill_add_row_form(row_to_add)
        self.browser.find_element(*webtables_loc.SUBMIT_BUTTON).click()


    def click_edit_button(self, row_number):
        button_locator = list(webtables_loc.EDIT_BUTTON)
        button_locator[1] += str(row_number)
        edit_button = self.browser.find_element(*button_locator)
        edit_button.click() 


    def clear_the_form(self):
        self.clear_input_located(*webtables_loc.FIRST_NAME_INPUT)
        self.clear_input_located(*webtables_loc.EMAIL_INPUT)
        self.clear_input_located(*webtables_loc.LAST_NAME_INPUT)
        self.clear_input_located(*webtables_loc.AGE_INPUT)
        self.clear_input_located(*webtables_loc.SALARY_INPUT)
        self.clear_input_located(*webtables_loc.DEPARTMENT_INPUT)


    def fill_add_row_form(self, row_to_add):
        self.fill_input_located(*webtables_loc.FIRST_NAME_INPUT, row_to_add['first name'])
        self.fill_input_located(*webtables_loc.LAST_NAME_INPUT, row_to_add['last name'])
        self.fill_input_located(*webtables_loc.EMAIL_INPUT, row_to_add['email'])
        self.fill_input_located(*webtables_loc.AGE_INPUT, row_to_add['age'])
        self.fill_input_located(*webtables_loc.SALARY_INPUT, row_to_add['salary'])
        self.fill_input_located(*webtables_loc.DEPARTMENT_INPUT, row_to_add['department'])


#BROKEN LINKS

    def image_is_broken(self, locator_type, locator):
        element = self.browser.find_element(locator_type, locator)
        width = element.get_attribute('naturalWidth')
        return int(width) == 0


#DYNAMIC PROPERTIES

    def is_clickable(self, locator_type,locator):
        try:
            self.wait.until(EC.element_to_be_clickable((locator_type, locator)))
            return True
        except:
            return False


    def is_visible(self, locator_type, locator):
        try:
            self.wait.until(EC.visibility_of_element_located((locator_type, locator)))
            return True
        except:
            return False


