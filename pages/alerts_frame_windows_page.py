from locators import accordian_service_loc
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.alert import Alert

from pages.base_page import BasePage


class AlertsFrameWindowsPage(BasePage):

    def __init__(self, browser: webdriver.Chrome, timeout=5, wait_timeout=5):
        super().__init__(browser, accordian_service_loc.ALERTS_FRAME_WINDOWS_LINK, timeout, wait_timeout)

    def alert_is_disappeared(self):
        try:
            Alert(self.browser).accept()
            return False
        except NoAlertPresentException:
            return True
