
from locators import mainpage_loc
from selenium import webdriver

from pages.base_page import BasePage


class MainPage(BasePage):

    def __init__(self, browser: webdriver.Chrome):
        super().__init__(browser, mainpage_loc.LINK)
