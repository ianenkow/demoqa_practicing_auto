from locators import accordian_service_loc
from selenium import webdriver

from pages.base_page import BasePage


class InterationsPage(BasePage):

    def __init__(self, browser: webdriver.Chrome, timeout=5, wait_timeout=5):
        super().__init__(browser, accordian_service_loc.INTERACTIONS_LINK, timeout, wait_timeout)


    def move_item(self, item_title, position_to_move):
        item_elements = self.browser.find_elements(*sortable_loc.SORTABLE_LIST_ITEMS)
        item_to_move = self.browser.find_element(By.XPATH, f"//*[contains(@class, 'vertical-list-container')]//div[text() = '{item_title}']")
        
        current_position = item_elements.index(item_to_move)
        if current_position == position_to_move:
            pass
        else:
            offset = (position_to_move - current_position)  * 50
            self.actions.click_and_hold(item_to_move).move_by_offset(0, offset).release().perform()
