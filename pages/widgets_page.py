
from locators import accordian_service_loc
from locators.widgets_locators import (accordian_loc, autocomplete_loc,
                                       datepicker_loc, slider_loc)
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from pages.base_page import BasePage


class WidgetsPage(BasePage):
    
    def __init__(self, browser: webdriver.Chrome, timeout=5, wait_timeout=5):
        super().__init__(browser, accordian_service_loc.WIDGETS_LINK, timeout, wait_timeout)


    def get_expanded_card_text(self):
        self.wait.until(EC.visibility_of_element_located((accordian_loc.EXPANDED_CARD_TEXT)))
        expanded_card_paragraphs = self.browser.find_elements(*accordian_loc.EXPANDED_CARD_TEXT)
        expanded_card_text = ''
        for paragraph in expanded_card_paragraphs:
            expanded_card_text += paragraph.text
        return expanded_card_text


    def choose_color(self, expected_text):
        input_options = self.browser.find_elements(*autocomplete_loc.INPUT_OPTIONS)
        for option in input_options:
            if option.text == expected_text:
                option.click()
                break



    def set_day(self, day):
        day_locator = f"//*[contains(@class,'react-datepicker__day')][text()={day}][not(contains(@class, 'outside-month'))]"
        self.browser.find_element(By.XPATH, day_locator).click()


    def set_month(self, month):
        month_select = self.browser.find_element(*datepicker_loc.MONTH_SELECT)
        month_select.click()
        months = self.browser.find_elements(*datepicker_loc.MONTHS_LIST)
        months[month-1].click()


    def set_year(self, year):
        year_select = self.browser.find_element(*datepicker_loc.YEAR_SELECT)
        year_select.click()
        years_elements = self.browser.find_elements(*datepicker_loc.YEARS_LIST)
        current_years_list = self.get_titles_list(years_elements)
        
        while int(current_years_list[-1]) > year:
            years_elements[-1].click()
            years_elements = self.browser.find_elements(*datepicker_loc.YEARS_LIST)
            current_years_list = self.get_titles_list(years_elements)

        year_locator = f"//div[contains(@class, 'react-datepicker__year-option')][text()={year}]"
        self.browser.find_element(By.XPATH, year_locator).click()  


    def set_date(self, date):
        date_list = str(date).split('.')
        self.set_year(int(date_list[2]))
        self.set_month(int(date_list[1]))
        self.set_day(int(date_list[0]))


    def set_time(self, time_to_set):
        time_locator = f"//*[contains(@class, 'time-list-item')][text()='{time_to_set}']"
        time_element = self.browser.find_element(By.XPATH, time_locator)
        self.actions.move_to_element(time_element).perform()
        time_element.click()


    def get_tab_text(self, tab_locator_type, tab_locator, tab_text_locator_type, tab_text_locator):
        tab = self.browser.find_element(tab_locator_type, tab_locator)
        tab.click()
        self.wait.until(EC.visibility_of_all_elements_located((tab_text_locator_type, tab_text_locator)))
        text = self.get_text_from_paragraphs_located(tab_text_locator_type, tab_text_locator)
        return text
