import time

from locators import accordian_service_loc
from locators.forms_locators import practice_form_loc
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.select import Select

from pages.base_page import BasePage


class FormsPage(BasePage):

    def __init__(self, browser: webdriver.Chrome, timeout=5):
        super().__init__(browser, accordian_service_loc.FORMS_LINK, timeout)


    #PRACTICE FORM

    def set_day(self, day):
        if day[0] == '0':
            day = day[1]
        day_locator = f"//*[contains(@class,'react-datepicker__day')][text()={day}][not(contains(@class, 'outside-month'))]"
        self.browser.find_element(By.XPATH, day_locator).click()


    def set_month(self, month):
        month_select = Select(self.browser.find_element(*practice_form_loc.MONTH_SELECT))
        month_select.select_by_value(str(int(month)-1))

    
    def set_year(self, year):
        year_select = Select(self.browser.find_element(*practice_form_loc.YEAR_SELECT))
        year_select.select_by_value(str(year))

    
    def set_date(self, date):
        date_list = str(date).split('.')
        self.set_year(date_list[2])
        self.set_month(date_list[1])
        self.set_day(date_list[0])


    def get_results(self):
        results_elements = self.browser.find_elements(*practice_form_loc.RESULTS)
        results = []
        for element in results_elements:
            if element.text == '':
                continue
            else:
                results.append(element.text)
        return results


    def element_is_invalid(self, element: WebElement, invalid_input_color):
        counter = 0
        while counter < 1:
            if str(element.value_of_css_property('border-color')) == invalid_input_color:
                return True
            else:
                counter += 0.05
                time.sleep(0.05)
        return False
        
        



    

    
