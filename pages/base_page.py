
import time
from abc import ABC

from locators import mainpage_loc
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class BasePage(ABC):

    def __init__(self, browser: webdriver.Chrome, url, timeout=5, wait_timeout=5):
        self.browser = browser
        self.url = url
        self.browser.implicitly_wait(timeout)
        self.wait = WebDriverWait(self.browser, wait_timeout, 0.05)
        self.actions = ActionChains(browser)

    def open(self):
        self.browser.get(self.url)


    def check_current_url(self, expected_url):
        actual_url = self.browser.current_url
        assert self.browser.current_url == expected_url, self.error_message('url', actual_url, expected_url)
        

    def main_logo_click(self):
        main_logo = self.browser.find_element(*mainpage_loc.MAIN_LOGO)
        main_logo.click()


    def scroll_to_avoid_footer(self, element):
        actions = ActionChains(self.browser)
        actions.move_to_element(element).perform()
        for integer in range(5):
            self.browser.find_element(By.CSS_SELECTOR, 'body').send_keys(Keys.ARROW_DOWN)
        time.sleep(0.5)


    def get_titles_list_located(self, elements_locator_type, elements_locator):
        element_list = self.browser.find_elements(elements_locator_type, elements_locator)
        titles = []
        for element in element_list:
            if element.text == '':
                continue
            else:
                titles.append(element.text)
        return titles


    def get_titles_list(self, element_list):
        titles = []
        for element in element_list:
            if element.text == '':
                continue
            else:
                titles.append(element.text)
        return titles


    def element_has_attribute_value(self, element: WebElement, attribute, expected_value):
        if expected_value in element.get_attribute(attribute).split():
            return True
        else:
            return False


    def fill_input(self, input: WebElement, text):
        input.send_keys(text)


    def fill_input_located(self, locator_type, locator, text):
        input = self.browser.find_element(locator_type, locator)
        self.fill_input(input, text)


    def clear_input(self, input: WebElement):
        input.send_keys(Keys.CONTROL + 'a')
        input.send_keys(Keys.DELETE)


    def clear_input_located(self, input_locator_type, input_locator):
        input = self.browser.find_element(input_locator_type, input_locator)
        self.clear_input(input)


    def switch_to_new_tab(self):
        new_window = self.browser.window_handles[-1]
        self.browser.switch_to.window(new_window)


    def error_message(self, type, actual_value, expected_value):
        return f'Wrong {type}! '\
            f'Actual: {actual_value} '\
            f'Expected: {expected_value}'


    def get_text_from_paragraphs_located(self, paragraphs_locator_type, paragraphs_locator):
        paragraph_elements = self.browser.find_elements(paragraphs_locator_type, paragraphs_locator)
        full_text = ''
        for element in paragraph_elements:
            full_text += element.text
        return full_text


    
    



